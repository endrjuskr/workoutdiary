﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Exercise.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.Core.Models
{
    #region

    using System.Collections.Generic;

    #endregion

    public class Exercise
    {
        public List<Photo> Photos;

        public string ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DifficultyLevel DifficultyLevel { get; set; }

        public Unit Unit { get; set; }

        public BodyPart BodyPart { get; set; }

        public string BodyPartImage
        {
            get
            {
                return "/Assets/BodyParts/" + BodyPart.ToString().ToLower() + ".jpg";
            }
        }

        public string DifficultyLevelText
        {
            get
            {
                return "Diffculty: " + this.DifficultyLevel;
            }
        }
    }

    public enum DifficultyLevel
    {
        Easy,

        Meddium,

        Hard
    }

    public enum BodyPart
    {
        Ab,

        Leg,

        Arm,
    }

    public enum Unit
    {
        Weight,

        Height,

        Distance,

        Quantity
    }
}