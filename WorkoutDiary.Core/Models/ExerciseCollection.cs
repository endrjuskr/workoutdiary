﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkoutDiary.Core.Models
{
    public class ExerciseCollection
    {
        public IList<Exercise> Exercises { get; set; } 
    }
}
