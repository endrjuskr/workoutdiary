﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Photo.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.Core.Models
{
    public class Photo
    {
        public string Title { get; set; }

        public string ImageUri { get; set; }

        public string ContainerName { get; set; }

        public string ResourceName { get; set; }

        public string SasQueryString { get; set; }
    }
}