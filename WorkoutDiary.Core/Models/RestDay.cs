﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RestDay.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.Core.Models
{
    #region

    using System;
    using System.Collections.Generic;

    #endregion

    public class RestDay
    {
        public string ID { get; set; }

        public string UserID { get; set; }

        public DateTime DateTime { get; set; }

        public int Rating { get; set; }

        public List<Measurment> Measurments { get; set; }

        public List<Photo> Photos { get; set; }

        public bool IsRestDay { get; set; }

        public string RatingText
        {
            get
            {
                return "Rating: " + (RatingEnum)this.Rating;
            }
        }

        public string Title
        {
            get
            {
                return (this.IsRestDay ? "Rest day: " : "Workout: ") + this.DateTime.ToLocalTime();
            }
        }
    }

    public class Measurment
    {
        public MeasurmentUnit Unit { get; set; }

        public double Value { get; set; }
    }

    public enum MeasurmentUnit
    {
        Weight,

        Height,

        HeartBits
    }

    public enum RatingEnum
    {
        Poor,
        Well,
        Excelent
    }
}