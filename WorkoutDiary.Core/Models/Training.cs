﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Training.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.Core.Models
{
    #region

    using System.Collections.Generic;

    #endregion

    public class Training
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public uint Duration { get; set; }

        public DifficultyLevel DifficultyLevel { get; set; }

        public List<TrainingPart> Parts { get; set; }

        public List<Photo> Photos { get; set; }

        public string DifficultyLevelText
        {
            get
            {
                return "Diffculty: " + this.DifficultyLevel;
            }
        }

        public string DurationText
        {
            get
            {
                return "Duration: " + this.Duration + " min";
            }
        }
    }

    public class TrainingPart
    {
        public string ExerciseID { get; set; }

        public uint Series { get; set; }

        public double Break { get; set; }

        public string SeriesText
        {
            get { return "Series count: " + this.Series; }
        }

        public string BreakText
        {
            get { return "Break: " + this.Break + " min"; }
        }

        public Exercise Exercise { get; set; }
    }
}