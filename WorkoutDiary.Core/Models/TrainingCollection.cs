﻿namespace WorkoutDiary.Core.Models
{
    using System.Collections.Generic;

    public class TrainingCollection
    {
        public IList<Training> Trainings { get; set; } 
    }
}
