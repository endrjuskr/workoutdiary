﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.Core.Models
{
    public class User
    {
        public ulong AuthID { get; set; }

        public string ID { get; set; }

        public string Name { get; set; }

        public string Photo { get; set; }

        public uint BirthYear { get; set; }

        public uint Weight { get; set; }

        public uint Height { get; set; }

        public AuthProvider AuthProvider { get; set; }
    }

    public enum AuthProvider
    {
        Twitter,

        Google
    }
}