﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Workout.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.Core.Models
{
    #region

    using System.Collections.Generic;
    using System.Linq;

    #endregion

    public class Workout : RestDay
    {
        public List<WorkoutExercise> Exercises { get; set; }
    }

    public class WorkoutExercise
    {
        public Exercise Exercise { get; set; }

        public string SeriesText
        {
            get { return "Series: " + string.Join(",", this.Series.Select(x => x.Count.ToString())); }
        }

        public string ExerciseID { get; set; }

        public List<WorkoutSerie> Series { get; set; }
    }

    public class WorkoutSerie
    {
        public int Count { get; set; }
    }
}