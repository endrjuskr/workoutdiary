﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkoutDiary.Core.Models
{
    public class WorkoutCollection
    {
        public IList<RestDay> Workouts { get; set; }
    }
}
