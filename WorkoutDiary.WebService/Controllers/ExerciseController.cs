﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExerciseController.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Controllers
{
    #region

    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using Microsoft.WindowsAzure.Mobile.Service;
    using Microsoft.WindowsAzure.Mobile.Service.Security;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WebService.Models;

    #endregion

    [AuthorizeLevel(AuthorizationLevel.User)]
    public class ExerciseController : ApiController
    {
        public ApiServices Services { get; set; }

        public IExerciseRepository Repository { get; set; }

        [HttpGet]
        [Route("api/exercise/getall")]
        public ExerciseCollection Get()
        {
            this.Repository = new ExerciseRepository();
            return new ExerciseCollection() { Exercises = this.Repository.GetAll().ToList() };
        }

        [HttpGet]
        public Exercise Get(string id)
        {
            this.Repository = new ExerciseRepository();
            return this.Repository.Get(id);
        }

        [HttpPost]
        public Exercise Insert(Exercise exercise)
        {
            this.Repository = new ExerciseRepository();
            return this.Repository.Create(exercise);
        }
    }
}