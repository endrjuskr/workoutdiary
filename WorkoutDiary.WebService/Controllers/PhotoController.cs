﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PhotoController.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Controllers
{
    #region

    using System;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Microsoft.WindowsAzure.Mobile.Service;
    using Microsoft.WindowsAzure.Mobile.Service.Security;
    using Microsoft.WindowsAzure.Storage.Auth;
    using Microsoft.WindowsAzure.Storage.Blob;

    using WorkoutDiary.Core.Models;

    #endregion

    [AuthorizeLevel(AuthorizationLevel.User)]
    public class PhotoController : ApiController
    {
        public ApiServices Services { get; set; }

        public async Task<Photo> Insert(Photo item)
        {
            string storageAccountName;
            string storageAccountKey;

            // Try to get the Azure storage account token from app settings.  
            if (
                !(Services.Settings.TryGetValue("STORAGE_ACCOUNT_NAME", out storageAccountName)
                  | Services.Settings.TryGetValue("STORAGE_ACCOUNT_ACCESS_KEY", out storageAccountKey)))
            {
                Services.Log.Error("Could not retrieve storage account settings.");
            }

            // Set the URI for the Blob Storage service.
            Uri blobEndpoint = new Uri(string.Format("https://{0}.blob.core.windows.net", storageAccountName));

            // Create the BLOB service client.
            CloudBlobClient blobClient = new CloudBlobClient(
                blobEndpoint,
                new StorageCredentials(storageAccountName, storageAccountKey));

            if (item.ContainerName != null)
            {
                // Set the BLOB store container name on the item, which must be lowercase.
                item.ContainerName = item.ContainerName.ToLower();

                // Create a container, if it doesn't already exist.
                CloudBlobContainer container = blobClient.GetContainerReference(item.ContainerName);
                await container.CreateIfNotExistsAsync();

                // Create a shared access permission policy. 
                BlobContainerPermissions containerPermissions = new BlobContainerPermissions();

                // Enable anonymous read access to BLOBs.
                containerPermissions.PublicAccess = BlobContainerPublicAccessType.Blob;
                container.SetPermissions(containerPermissions);

                // Define a policy that gives write access to the container for 5 minutes.                                   
                SharedAccessBlobPolicy sasPolicy = new SharedAccessBlobPolicy
                                                   {
                                                       SharedAccessStartTime = DateTime.UtcNow,
                                                       SharedAccessExpiryTime =
                                                           DateTime.UtcNow.AddMinutes(5),
                                                       Permissions =
                                                           SharedAccessBlobPermissions.Write
                                                   };

                // Get the SAS as a string.
                item.SasQueryString = container.GetSharedAccessSignature(sasPolicy);

                // Set the URL used to store the image.
                item.ImageUri = string.Format("{0}{1}/{2}", blobEndpoint, item.ContainerName, item.ResourceName);
            }

            return item;
        }
    }
}