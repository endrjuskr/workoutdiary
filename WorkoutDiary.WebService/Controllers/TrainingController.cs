﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingController.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Controllers
{
    #region

    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using Microsoft.WindowsAzure.Mobile.Service;
    using Microsoft.WindowsAzure.Mobile.Service.Security;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WebService.Models;

    #endregion

    [AuthorizeLevel(AuthorizationLevel.User)]
    public class TrainingController : ApiController
    {
        public ApiServices Services { get; set; }

        public ITrainingRepository Repository { get; set; }

        [HttpGet]
        [Route("api/training/getall")]
        public TrainingCollection Get()
        {
            this.Repository = new TrainingRepository();
            return new TrainingCollection() { Trainings = this.Repository.GetAll().ToList() };
        }

        [HttpGet]
        public Training Get(string id)
        {
            this.Repository = new TrainingRepository();
            return this.Repository.Get(id);
        }

        [HttpPost]
        public Training Insert(Training training)
        {
            this.Repository = new TrainingRepository();
            return this.Repository.Create(training);
        }
    }
}