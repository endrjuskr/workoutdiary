﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserController.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Controllers
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.ServiceModel.Dispatcher;
    using System.Web.Http;

    using Microsoft.WindowsAzure.Mobile.Service;
    using Microsoft.WindowsAzure.Mobile.Service.Security;

    using Newtonsoft.Json.Linq;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WebService.Models;

    #endregion

    [AuthorizeLevel(AuthorizationLevel.User)]
    public class UserController : ApiController
    {

        public ApiServices Services { get; set; }

        public IUserRepository Repository { get; set; }

        [HttpGet]
        [Route("api/user/hello")]
        public string Hello()
        {
            return "Hello";
        }

        [HttpGet]
        [Route("api/user/checkuser")]
        public User CheckUser(string authID)
        {
            this.Repository = new UserRepository();
            return this.Repository.CheckUser(authID);
        }

        [HttpPost]
        [Route("api/user/insert")]
        public User Insert(User user)
        {
            this.Repository = new UserRepository();
            if ((this.ModelState.IsValid) && (user != null))
            {
                var newUser =  this.Repository.Create(user);
                return newUser;
            }

            return null;
        }
    }
}