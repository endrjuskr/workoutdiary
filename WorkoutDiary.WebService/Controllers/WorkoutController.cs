﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutController.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System.Web;

namespace WorkoutDiary.WebService.Controllers
{
    #region

    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using Microsoft.WindowsAzure.Mobile.Service;
    using Microsoft.WindowsAzure.Mobile.Service.Security;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WebService.Models;

    #endregion

    [AuthorizeLevel(AuthorizationLevel.User)]
    public class WorkoutController : ApiController
    {
        public ApiServices Services { get; set; }

        public IWorkoutRepository Repository { get; set; }

        [HttpGet]
        [Route("api/workout/getall")]
        public WorkoutCollection GetAll(string id)
        {
            this.Repository = new WorkoutRepository();
            return new WorkoutCollection() { Workouts = this.Repository.GetAll(id).ToList() };
        }

        [HttpGet]
        [Route("api/workout/getworkout")]
        public Workout GetWorkout(string id)
        {
            this.Repository = new WorkoutRepository();
            return this.Repository.GetWorkout(id);
        }

        [HttpGet]
        [Route("api/workout/getrestday")]
        public RestDay GetRestDay(string id)
        {
            this.Repository = new WorkoutRepository();
            return this.Repository.GetRestDay(id);
        }

        [HttpPost]
        [Route("api/workout/insertrestday")]
        public RestDay InsertRestDay(RestDay restDay)
        {
            this.Repository = new WorkoutRepository();
            return this.Repository.Create(restDay);
        }

        [HttpPost]
        [Route("api/workout/insertworkout")]
        public Workout InsertWorkout(Workout workout)
        {
            this.Repository = new WorkoutRepository();
            return this.Repository.Create(workout);
        }
    }
}