﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService
{
    #region

    using System.Web;

    #endregion

    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            WebApiConfig.Register();
        }
    }
}