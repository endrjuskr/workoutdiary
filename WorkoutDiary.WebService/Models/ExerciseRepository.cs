﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExerciseRepository.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Models
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using WorkoutDiary.Core.Models;

    #endregion

    public class ExerciseRepository : IExerciseRepository
    {
        private readonly XDocument xmlDocument;

        private readonly string xmlFilename;

        public ExerciseRepository()
        {
            try
            {
                this.xmlFilename = @"D:\home\site\wwwroot\bin\App_Data\WorkoutDiaryDatabase.xml";
                this.xmlDocument = XDocument.Load(this.xmlFilename);
            }
            catch (Exception ex)
            {
                // Rethrow the exception.
                throw ex;
            }
        }

        public Exercise Create(Exercise exercise)
        {
            try
            {
                var highestUser = (from exerciseNode in this.xmlDocument.Element("database").Element("exercises").Elements("exercise")
                                   orderby exerciseNode.Attribute("id").Value descending
                                   select exerciseNode).Take(1);
                var highestId = highestUser.Attributes("id").First().Value;
                var newId = (Convert.ToInt32(highestId) + 1).ToString();
                if (this.Get(newId) == null)
                {
                    var exercises = this.xmlDocument.Element("database").Elements("exercises").Single();
                    var newExercise = new XElement("exercise", new XAttribute("id", newId));
                    var exerciseInfo = this.FormatExerciseData(exercise);
                    newExercise.ReplaceNodes(exerciseInfo);
                    exercises.Add(newExercise);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.Get(newId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public IEnumerable<Exercise> GetAll()
        {
            try
            {
                return (from exercise in this.xmlDocument.Element("database").Element("exercises").Elements("exercise")
                        orderby exercise.Attribute("id").Value ascending
                        select
                            new Exercise
                            {
                                ID = exercise.Attribute("id").Value,
                                Name = exercise.Element("name").Value,
                                Description = exercise.Element("description").Value,
                                Photos = this.GetAllPhotos(exercise.Element("photos")),
                                DifficultyLevel =
                                    (DifficultyLevel)
                                    Enum.Parse(
                                        typeof(DifficultyLevel),
                                        exercise.Element("difficulty-level").Value),
                                Unit = (Unit)Enum.Parse(typeof(Unit), exercise.Element("unit").Value),
                                BodyPart = (BodyPart)Enum.Parse(typeof(BodyPart), exercise.Element("body-part").Value)
                            })
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Exercise Get(string id)
        {
            try
            {
                return (from exercise in this.xmlDocument.Element("database").Element("exercises").Elements("exercise")
                        where exercise.Attribute("id").Value.Equals(id)
                        select
                            new Exercise
                            {
                                ID = exercise.Attribute("id").Value,
                                Name = exercise.Element("name").Value,
                                Description = exercise.Element("description").Value,
                                Photos = this.GetAllPhotos(exercise.Element("photos")),
                                DifficultyLevel =
                                    (DifficultyLevel)
                                    Enum.Parse(
                                        typeof(DifficultyLevel),
                                        exercise.Element("difficulty-level").Value),
                                Unit = (Unit)Enum.Parse(typeof(Unit), exercise.Element("unit").Value),
                                BodyPart = (BodyPart)Enum.Parse(typeof(BodyPart), exercise.Element("body-part").Value)
                            })
                    .Single();
            }
            catch
            {
                return null;
            }
        }

        public Exercise Update(String id, Exercise exercise)
        {
            try
            {
                var updateExercise = this.xmlDocument.XPathSelectElement(String.Format("database/exercises/exercise[@id='{0}']", id));
                if (updateExercise != null)
                {
                    var exerciseInfo = this.FormatExerciseData(exercise);
                    updateExercise.ReplaceNodes(exerciseInfo);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.Get(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public Boolean Delete(String id)
        {
            try
            {
                if (this.Get(id) == null)
                {
                    return false;
                }

                this.xmlDocument.Element("database")
                    .Element("exercises")
                    .Elements("exercise")
                    .Where(x => x.Attribute("id").Value.Equals(id))
                    .Remove();
                this.xmlDocument.Save(this.xmlFilename);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<Photo> GetAllPhotos(XElement photos)
        {
            return (from photo in photos.Elements("photo")
                    select new Photo { Title = photo.Element("title").Value, ImageUri = photo.Element("image").Value })
                .ToList();
        }

        private XElement[] FormatExerciseData(Exercise exercise)
        {
            var photosInfo = exercise.Photos.Select(
                p =>
                {
                    var photo = new XElement("photo");
                    var photoInfo = this.FormatPhotoData(p);
                    photo.ReplaceNodes(photoInfo);
                    return photo;
                }).ToList();
            var photos = new XElement("photos");
            photos.ReplaceNodes(photosInfo);
            XElement[] exerciseInfo =
            {
                new XElement("name", exercise.Name), 
                new XElement("description", exercise.Description),
                new XElement("difficulty-level", exercise.DifficultyLevel.ToString()),
                new XElement("unit", exercise.Unit.ToString()),
                new XElement("body-part", exercise.BodyPart.ToString()), 
                photos
            };
            return exerciseInfo;
        }

        private XElement[] FormatPhotoData(Photo photo)
        {
            XElement[] photoInfo = { new XElement("title", photo.Title), new XElement("image", photo.ImageUri) };
            return photoInfo;
        }
    }
}