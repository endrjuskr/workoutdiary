﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IExerciseRepository.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Models
{
    #region

    using System.Collections.Generic;

    using WorkoutDiary.Core.Models;

    #endregion

    public interface IExerciseRepository
    {
        Exercise Create(Exercise exercise);

        IEnumerable<Exercise> GetAll();

        Exercise Get(string exerciseID);

        Exercise Update(string exerciseID, Exercise exercise);

        bool Delete(string exerciseID);
    }
}