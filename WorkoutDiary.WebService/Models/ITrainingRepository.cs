﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITrainingRepository.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Models
{
    #region

    using System.Collections.Generic;

    using WorkoutDiary.Core.Models;

    #endregion

    public interface ITrainingRepository
    {
        Training Create(Training training);

        IEnumerable<Training> GetAll();

        Training Get(string trainingID);

        Training Update(string trainingID, Training training);

        bool Delete(string trainingID);
    }
}