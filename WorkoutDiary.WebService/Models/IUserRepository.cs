﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserRepository.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Models
{
    #region

    using System.Collections.Generic;

    using WorkoutDiary.Core.Models;

    #endregion

    public interface IUserRepository
    {
        User Create(User user);

        User CheckUser(string authID);

        IEnumerable<User> GetAll();

        User Get(string userID);

        User Update(string userID, User user);

        bool Delete(string userID);
    }
}