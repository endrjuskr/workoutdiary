﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWorkoutRepository.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Models
{
    #region

    using System.Collections.Generic;

    using WorkoutDiary.Core.Models;

    #endregion

    public interface IWorkoutRepository
    {
        Workout Create(Workout workout);

        RestDay Create(RestDay restDay);

        IEnumerable<RestDay> GetAll(string id);

        Workout GetWorkout(string workoutID);

        RestDay GetRestDay(string restDayID);

        Workout UpdateWorkout(string workoutID, Workout workout);

        RestDay UpdateRestDay(string restDayID, RestDay restDay);

        bool Delete(string workoutID);
    }
}