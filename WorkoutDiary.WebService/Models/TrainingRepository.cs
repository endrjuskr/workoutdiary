﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingRepository.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Models
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using WorkoutDiary.Core.Models;

    #endregion

    public class TrainingRepository : ITrainingRepository
    {
        private readonly XDocument xmlDocument;

        private readonly string xmlFilename;

        public TrainingRepository()
        {
            try
            {
                this.xmlFilename = @"D:\home\site\wwwroot\bin\App_Data\WorkoutDiaryDatabase.xml";
                this.xmlDocument = XDocument.Load(this.xmlFilename);
            }
            catch (Exception ex)
            {
                // Rethrow the exception.
                throw ex;
            }
        }

        public Training Create(Training training)
        {
            try
            {
                var highestUser = (from exerciseNode in this.xmlDocument.Element("database").Element("trainings").Elements("training")
                                   orderby exerciseNode.Attribute("id").Value descending
                                   select exerciseNode).Take(1);
                var highestId = highestUser.Attributes("id").First().Value;
                var newId = (Convert.ToInt32(highestId) + 1).ToString();
                if (this.Get(newId) == null)
                {
                    var trainings = this.xmlDocument.Element("database").Elements("trainings").Single();
                    var newTraining = new XElement("training", new XAttribute("id", newId));
                    var trainingInfo = this.FormatTrainingData(training);
                    newTraining.ReplaceNodes(trainingInfo);
                    trainings.Add(newTraining);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.Get(newId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public IEnumerable<Training> GetAll()
        {
            try
            {
                return (from training in this.xmlDocument.Element("database").Element("trainings").Elements("training")
                        orderby training.Attribute("id").Value ascending
                        select
                            new Training
                            {
                                ID = training.Attribute("id").Value,
                                Name = training.Element("name").Value,
                                Description = training.Element("description").Value,
                                Duration = Convert.ToUInt32(training.Element("duration").Value),
                                Photos = this.GetAllPhotos(training.Element("photos")),
                                DifficultyLevel =
                                    (DifficultyLevel)
                                    Enum.Parse(
                                        typeof(DifficultyLevel),
                                        training.Element("difficulty-level").Value),
                                Parts = this.GetAllParts(training.Element("parts"))
                            }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Training Get(string id)
        {
            try
            {
                return (from training in this.xmlDocument.Element("database").Element("trainings").Elements("training")
                        where training.Attribute("id").Value.Equals(id)
                        select
                            new Training
                            {
                                ID = training.Attribute("id").Value,
                                Name = training.Element("name").Value,
                                Description = training.Element("description").Value,
                                Duration = Convert.ToUInt32(training.Element("duration").Value),
                                Photos = this.GetAllPhotos(training.Element("photos")),
                                DifficultyLevel =
                                    (DifficultyLevel)
                                    Enum.Parse(
                                        typeof(DifficultyLevel),
                                        training.Element("difficulty-level").Value),
                                Parts = this.GetAllParts(training.Element("parts"))
                            }).Single();
            }
            catch
            {
                return null;
            }
        }

        public Training Update(String id, Training training)
        {
            try
            {
                var updateTraining = this.xmlDocument.XPathSelectElement(String.Format("database/trainings/training[@id='{0}']", id));
                if (updateTraining != null)
                {
                    var trainingInfo = this.FormatTrainingData(training);
                    updateTraining.ReplaceNodes(trainingInfo);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.Get(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public Boolean Delete(String id)
        {
            try
            {
                if (this.Get(id) == null)
                {
                    return false;
                }

                this.xmlDocument.Element("database")
                    .Element("trainings")
                    .Elements("training")
                    .Where(x => x.Attribute("id").Value.Equals(id))
                    .Remove();
                this.xmlDocument.Save(this.xmlFilename);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<Photo> GetAllPhotos(XElement photos)
        {
            return (from photo in photos.Elements("photo")
                    select new Photo { Title = photo.Element("title").Value, ImageUri = photo.Element("image").Value })
                .ToList();
        }

        private List<TrainingPart> GetAllParts(XElement parts)
        {
            return (from part in parts.Elements("part")
                    select
                        new TrainingPart
                        {
                            ExerciseID = part.Element("exercise-id").Value,
                            Break = Convert.ToDouble(part.Element("break").Value),
                            Series = Convert.ToUInt32(part.Element("series").Value)
                        }).ToList();
        }

        private XElement[] FormatTrainingData(Training training)
        {
            var photosInfo = training.Photos.Select(
                p =>
                {
                    var photo = new XElement("photo");
                    var photoInfo = this.FormatPhotoData(p);
                    photo.ReplaceNodes(photoInfo);
                    return photo;
                }).ToList();
            var partsInfo = training.Parts.Select(
                p =>
                {
                    var part = new XElement("part");
                    var partInfo = this.FormatPartData(p);
                    part.ReplaceNodes(partInfo);
                    return part;
                }).ToList();
            var photos = new XElement("photos");
            photos.ReplaceNodes(photosInfo);
            var parts = new XElement("parts");
            parts.ReplaceNodes(partsInfo);

            XElement[] exerciseInfo =
            {
                new XElement("name", training.Name),
                new XElement("description", training.Description),
                new XElement("duration", training.Duration),
                new XElement("difficulty-level", training.DifficultyLevel.ToString()), photos,
                parts
            };
            return exerciseInfo;
        }

        private XElement[] FormatPhotoData(Photo photo)
        {
            XElement[] photoInfo = { new XElement("title", photo.Title), new XElement("image", photo.ImageUri) };
            return photoInfo;
        }

        private XElement[] FormatPartData(TrainingPart part)
        {
            XElement[] partInfo =
            {
                new XElement("exercise-id", part.ExerciseID),
                new XElement("series", part.Series),
                new XElement("break", part.Break)
            };
            return partInfo;
        }
    }
}