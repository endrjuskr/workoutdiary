﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Models
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using WorkoutDiary.Core.Models;

    #endregion

    public class UserRepository : IUserRepository
    {
        private readonly XDocument xmlDocument;

        private readonly string xmlFilename;

        public UserRepository()
        {
            try
            {
                this.xmlFilename = @"D:\home\site\wwwroot\bin\App_Data\WorkoutDiaryDatabase.xml";
                this.xmlDocument = XDocument.Load(this.xmlFilename);
            }
            catch (Exception ex)
            {
                // Rethrow the exception.
                throw ex;
            }
        }

        public User Create(User user)
        {
            try
            {
                var highestUser = (from userNode in this.xmlDocument.Element("database").Element("users").Elements("user")
                                   orderby userNode.Attribute("id").Value descending
                                   select userNode).Take(1);
                var highestId = highestUser.Attributes("id").First().Value;
                var newId = (Convert.ToInt32(highestId) + 1).ToString();
                if (this.Get(newId) == null)
                {
                    var users = this.xmlDocument.Element("database").Elements("users").Single();
                    var newUser = new XElement("user", new XAttribute("id", newId));
                    var userInfo = this.FormatUserData(user);
                    newUser.ReplaceNodes(userInfo);
                    users.Add(newUser);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.Get(newId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public User CheckUser(string authID)
        {
            var k = (from userNode in this.xmlDocument.Elements("database").Elements("users").Elements("user")
                    where userNode.Element("auth-id").Value.Equals(authID)
                    select userNode).Any();
            if (k)
            {
                var t = (from userNode in this.xmlDocument.Elements("database").Elements("users").Elements("user")
                         where userNode.Element("auth-id").Value.Equals(authID)
                         select userNode.Attribute("id").Value).Single();
                return this.Get(t);
            }
            return null;
        }

        public IEnumerable<User> GetAll()
        {
            try
            {
                return (from user in this.xmlDocument.Elements("database").Elements("users").Elements("user")
                        orderby user.Attribute("id").Value ascending
                        select
                            new User
                            {
                                ID = user.Attribute("id").Value,
                                Name = user.Element("name").Value,
                                BirthYear = Convert.ToUInt32(user.Element("birth-year").Value),
                                Photo = user.Element("icon").Value,
                                AuthID = Convert.ToUInt32(user.Element("auth-id").Value),
                                AuthProvider = AuthProvider.Twitter,
                                Weight = Convert.ToUInt32(user.Element("weight").Value),
                                Height = Convert.ToUInt32(user.Element("height").Value)
                            }).ToList();
            }
            catch (Exception ex)
            {
                // Rethrow the exception.
                throw ex;
            }
        }

        public User Get(string id)
        {
            try
            {
                return (from user in this.xmlDocument.Element("database").Element("users").Elements("user")
                        where user.Attribute("id").Value.Equals(id)
                        select
                            new User
                            {
                                ID = user.Attribute("id").Value,
                                Name = user.Element("name").Value,
                                BirthYear = Convert.ToUInt32(user.Element("birth-year").Value),
                                Photo = user.Element("icon").Value,
                                AuthID = Convert.ToUInt32(user.Element("auth-id").Value),
                                AuthProvider = AuthProvider.Twitter,
                                Weight = Convert.ToUInt32(user.Element("weight").Value),
                                Height = Convert.ToUInt32(user.Element("height").Value)
                            }).Single();
            }
            catch
            {
                return null;
            }
        }

        public User Update(String id, User user)
        {
            try
            {
                var updateBook = this.xmlDocument.XPathSelectElement(String.Format("database/users/user[@id='{0}']", id));
                if (updateBook != null)
                {
                    var userInfo = this.FormatUserData(user);
                    updateBook.ReplaceNodes(userInfo);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.Get(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public Boolean Delete(String id)
        {
            try
            {
                if (this.Get(id) != null)
                {
                    this.xmlDocument.Elements("database")
                        .Elements("users")
                        .Elements("user")
                        .Where(x => x.Attribute("id").Value.Equals(id))
                        .Remove();
                    this.xmlDocument.Save(this.xmlFilename);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private XElement[] FormatUserData(User user)
        {
            XElement[] userInfo =
            {
                new XElement("name", user.Name), 
                new XElement("icon", user.Photo), 
                new XElement("weight", user.Weight),
                new XElement("height", user.Height), 
                new XElement("birth-year", user.BirthYear),
                new XElement("auth-id", user.AuthID),
                new XElement("auth-provider", AuthProvider.Twitter.ToString())
            };
            return userInfo;
        }
    }
}