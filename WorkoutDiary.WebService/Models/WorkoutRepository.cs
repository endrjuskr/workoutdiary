﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutRepository.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.Models
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using WorkoutDiary.Core.Models;

    #endregion

    public class WorkoutRepository : IWorkoutRepository
    {
        private readonly XDocument xmlDocument;

        private readonly string xmlFilename;

        public WorkoutRepository()
        {
            try
            {
                this.xmlFilename = @"D:\home\site\wwwroot\bin\App_Data\WorkoutDiaryDatabase.xml";
                this.xmlDocument = XDocument.Load(this.xmlFilename);
            }
            catch (Exception ex)
            {
                // Rethrow the exception.
                throw ex;
            }
        }

        public Workout Create(Workout workout)
        {
            try
            {
                var highestUser = (from exerciseNode in this.xmlDocument.Element("database").Element("workouts").Elements()
                                   orderby exerciseNode.Attribute("id").Value descending
                                   select exerciseNode).Take(1);
                var highestId = highestUser.Attributes("id").First().Value;
                var newId = (Convert.ToInt32(highestId) + 1).ToString();
                if (this.GetWorkout(newId) == null)
                {
                    var workouts = this.xmlDocument.Element("database").Elements("workouts").Single();
                    var newWorkout = new XElement("workout", new XAttribute("id", newId));
                    var workoutInfo = this.FormatWorkoutData(workout);
                    newWorkout.ReplaceNodes(workoutInfo);
                    workouts.Add(newWorkout);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.GetWorkout(newId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public RestDay Create(RestDay restDay)
        {
            try
            {
                var highestUser = (from workoutNode in this.xmlDocument.Element("database").Element("workouts").Elements()
                                   orderby workoutNode.Attribute("id").Value descending
                                   select workoutNode).Take(1);
                var highestId = highestUser.Attributes("id").First().Value;
                var newId = (Convert.ToInt32(highestId) + 1).ToString();
                if (this.GetRestDay(newId) == null)
                {
                    var workouts = this.xmlDocument.Element("database").Elements("workouts").Single();
                    var newRestDay = new XElement("rest-day", new XAttribute("id", newId));
                    var restDayInfo = this.FormatRestDayData(restDay);
                    newRestDay.ReplaceNodes(restDayInfo);
                    workouts.Add(newRestDay);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.GetRestDay(newId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public IEnumerable<RestDay> GetAll(string id)
        {
            try
            {
                return (from restDay in this.xmlDocument.Elements("database").Elements("workouts").Elements()
                        where restDay.Element("user-id").Value.Equals(id)
                        orderby restDay.Attribute("id").Value ascending
                        select
                            new RestDay
                            {
                                ID = restDay.Attribute("id").Value,
                                DateTime = Convert.ToDateTime(restDay.Element("datetime").Value),
                                UserID = restDay.Element("user-id").Value,
                                Rating = Convert.ToInt32(restDay.Element("rating").Value),
                                Photos = this.GetAllPhotos(restDay.Element("photos")),
                                Measurments = this.GetAllMeasurments(restDay.Element("measurments")),
                                IsRestDay = restDay.Name == "rest-day"
                            }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RestDay GetRestDay(string id)
        {
            try
            {
                return (from restDay in this.xmlDocument.Elements("database").Elements("workouts").Elements("rest-day")
                        where restDay.Attribute("id").Value.Equals(id)
                        select
                            new RestDay
                            {
                                ID = restDay.Attribute("id").Value,
                                DateTime = Convert.ToDateTime(restDay.Element("datetime").Value),
                                UserID = restDay.Element("user-id").Value,
                                Rating = Convert.ToInt32(restDay.Element("rating").Value),
                                Photos = this.GetAllPhotos(restDay.Element("photos")),
                                Measurments = this.GetAllMeasurments(restDay.Element("measurments"))
                            }).Single
                    ();
            }
            catch
            {
                return null;
            }
        }

        public Workout GetWorkout(string id)
        {
            try
            {
                return (from workout in this.xmlDocument.Elements("database").Elements("workouts").Elements("workout")
                        where workout.Attribute("id").Value.Equals(id)
                        select
                            new Workout
                            {
                                ID = workout.Attribute("id").Value,
                                DateTime = Convert.ToDateTime(workout.Element("datetime").Value),
                                UserID = workout.Element("user-id").Value,
                                Rating = Convert.ToInt32(workout.Element("rating").Value),
                                Photos = this.GetAllPhotos(workout.Element("photos")),
                                Measurments = this.GetAllMeasurments(workout.Element("measurments")),
                                Exercises = this.GetAllExercises(workout.Element("exercises"))
                            }).Single();
            }
            catch
            {
                return null;
            }
        }

        public Workout UpdateWorkout(String id, Workout workout)
        {
            try
            {
                var updateWorkout = this.xmlDocument.XPathSelectElement(String.Format("database/workouts/workout[@id='{0}']", id));
                if (updateWorkout != null)
                {
                    var workoutInfo = this.FormatWorkoutData(workout);
                    updateWorkout.ReplaceNodes(workoutInfo);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.GetWorkout(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public RestDay UpdateRestDay(String id, RestDay restDay)
        {
            try
            {
                var updateRestDay = this.xmlDocument.XPathSelectElement(String.Format("database/workouts/rest-day[@id='{0}']", id));
                if (updateRestDay != null)
                {
                    var restDayInfo = this.FormatRestDayData(restDay);
                    updateRestDay.ReplaceNodes(restDayInfo);
                    this.xmlDocument.Save(this.xmlFilename);
                    return this.GetWorkout(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public Boolean Delete(String id)
        {
            try
            {
                if (this.GetWorkout(id) != null)
                {
                    this.xmlDocument.Elements("database")
                        .Elements("workouts")
                        .Elements("workout")
                        .Where(x => x.Attribute("id").Value.Equals(id))
                        .Remove();
                    this.xmlDocument.Save(this.xmlFilename);
                    return true;
                }
                if (this.GetRestDay(id) != null)
                {
                    this.xmlDocument.Elements("database")
                        .Elements("workouts")
                        .Elements("rest-day")
                        .Where(x => x.Attribute("id").Value.Equals(id))
                        .Remove();
                    this.xmlDocument.Save(this.xmlFilename);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<Photo> GetAllPhotos(XElement photos)
        {
            return (from photo in photos.Elements("photo")
                    select new Photo { Title = photo.Element("title").Value, ImageUri = photo.Element("image").Value })
                .ToList();
        }

        private List<Measurment> GetAllMeasurments(XElement measurments)
        {
            return (from measurment in measurments.Elements("measurment")
                    select
                        new Measurment
                        {
                            Unit =
                                (MeasurmentUnit)
                                Enum.Parse(typeof(MeasurmentUnit), measurment.Element("unit").Value),
                            Value = Convert.ToDouble(measurment.Element("value").Value)
                        }).ToList();
        }

        private List<WorkoutExercise> GetAllExercises(XElement exercises)
        {
            return (from exercise in exercises.Elements("exercise")
                    select
                        new WorkoutExercise
                        {
                            ExerciseID = exercise.Element("exercise-id").Value,
                            Series = this.GetAllSeries(exercise.Element("series"))
                        }).ToList();
        }

        private List<WorkoutSerie> GetAllSeries(XElement series)
        {
            return (from serie in series.Elements("serie")
                    select new WorkoutSerie { Count = Convert.ToInt32(serie.Attribute("count").Value) }).ToList();
        }

        private XElement[] FormatWorkoutData(Workout workout)
        {
            var photosInfo = workout.Photos.Select(
                p =>
                {
                    var photo = new XElement("photo");
                    var photoInfo = this.FormatPhotoData(p);
                    photo.ReplaceNodes(photoInfo);
                    return photo;
                }).ToList();
            var measurmentsInfo = workout.Measurments.Select(
                p =>
                {
                    var measurment = new XElement("measurment");
                    var measurmentInfo = this.FormatMeasurmentData(p);
                    measurment.ReplaceNodes(measurmentInfo);
                    return measurment;
                }).ToList();
            var exercisesInfo = workout.Exercises.Select(
                p =>
                {
                    var exercise = new XElement("exercise");
                    var exerciseInfo = this.FormatExerciseData(p);
                    exercise.ReplaceNodes(exerciseInfo);
                    return exercise;
                }).ToList();

            var photos = new XElement("photos");
            photos.ReplaceNodes(photosInfo);
            var exercises = new XElement("exercises");
            exercises.ReplaceNodes(exercisesInfo);
            var measurments = new XElement("measurments");
            measurments.ReplaceNodes(measurmentsInfo);

            XElement[] workoutInfo =
            {
                new XElement("user-id", workout.UserID), 
                new XElement("rating", workout.Rating),
                new XElement("datetime", workout.DateTime),
                photos,
                measurments,
                exercises
            };

            return workoutInfo;
        }

        private XElement[] FormatRestDayData(RestDay restDay)
        {
            var photosInfo = restDay.Photos.Select(
                p =>
                {
                    var photo = new XElement("photo");
                    var photoInfo = this.FormatPhotoData(p);
                    photo.ReplaceNodes(photoInfo);
                    return photo;
                }).ToList();
            var measurmentsInfo = restDay.Measurments.Select(
                p =>
                {
                    var measurment = new XElement("measurment");
                    var measurmentInfo = this.FormatMeasurmentData(p);
                    measurment.ReplaceNodes(measurmentInfo);
                    return measurment;
                }).ToList();

            var photos = new XElement("photos");
            photos.ReplaceNodes(photosInfo);
            var measurments = new XElement("measurments");
            measurments.ReplaceNodes(measurmentsInfo);

            XElement[] restDayInfo =
            {
                new XElement("user-id", restDay.UserID), new XElement("rating", restDay.Rating),
                new XElement("datetime", restDay.DateTime), photos, measurments
            };

            return restDayInfo;
        }

        private XElement[] FormatPhotoData(Photo photo)
        {
            XElement[] photoInfo = { new XElement("title", photo.Title), new XElement("image", photo.ImageUri) };
            return photoInfo;
        }

        private XElement[] FormatMeasurmentData(Measurment measurment)
        {
            XElement[] measurmentInfo =
            {
                new XElement("unit", measurment.Unit), new XElement("value", measurment.Value)
            };
            return measurmentInfo;
        }

        private XElement[] FormatExerciseData(WorkoutExercise exercise)
        {
            var seriesInfo =
                exercise.Series.Select(p => new XElement("serie", new XAttribute("count", p.Count))).ToList();
            var series = new XElement("series");
            series.ReplaceNodes(seriesInfo);

            XElement[] exerciseInfo = { new XElement("exercise-id", exercise.ExerciseID), series };
            return exerciseInfo;
        }
    }
}