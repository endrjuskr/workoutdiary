﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SampleJob.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WebService.ScheduledJobs
{
    #region

    using System.Threading.Tasks;
    using System.Web.Http;

    using Microsoft.WindowsAzure.Mobile.Service;

    #endregion

    // A simple scheduled job which can be invoked manually by submitting an HTTP
    // POST request to the path "/jobs/sample".

    public class SampleJob : ScheduledJob
    {
        public override Task ExecuteAsync()
        {
            this.Services.Log.Info("Hello from scheduled job!");
            return Task.FromResult(true);
        }
    }
}