// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppBootstrapper.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Windows.Controls;

    using Caliburn.Micro;
    using Caliburn.Micro.BindableAppBar;

    using Microsoft.Phone.Controls;

    using Telerik.Windows.Controls;

    using WorkoutDiary.WindowsPhoneApp.Services;
    using WorkoutDiary.WindowsPhoneApp.ViewModels;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Training;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Workout;

    #endregion

    public class AppBootstrapper : PhoneBootstrapperBase
    {
        private PhoneContainer container;

        public AppBootstrapper()
        {
            this.Start();
        }

        protected override void Configure()
        {
            this.container = new PhoneContainer();
            if (!Execute.InDesignMode)
            {
                this.container.RegisterPhoneServices(this.RootFrame);
            }

            this.container.PerRequest<LoginViewModel>();
            this.container.PerRequest<MenuViewModel>();
            this.container.PerRequest<ExerciseListViewModel>();
            this.container.PerRequest<TrainingListViewModel>();
            this.container.PerRequest<WorkoutListViewModel>();
            this.container.PerRequest<ExerciseViewModel>();
            this.container.PerRequest<ExercisePhotoViewModel>();
            this.container.PerRequest<ExerciseDetailViewModel>();
            this.container.PerRequest<TrainingViewModel>();
            this.container.PerRequest<TrainingPhotoViewModel>();
            this.container.PerRequest<TrainingDetailViewModel>();
            this.container.PerRequest<TrainingExerciseViewModel>();
            this.container.PerRequest<TrainingNewExerciseViewModel>();
            this.container.PerRequest<WorkoutViewModel>();
            this.container.PerRequest<WorkoutPhotoViewModel>();
            this.container.PerRequest<WorkoutDetailViewModel>();
            this.container.PerRequest<WorkoutExerciseViewModel>();
            this.container.PerRequest<WorkoutMeasurmentViewModel>();
            this.container.PerRequest<WorkoutNewExerciseViewModel>();
            this.container.PerRequest<RestDayViewModel>();
            this.container.Singleton<IUserService, UserService>();
            this.container.Singleton<IExerciseService, ExerciseService>();
            this.container.Singleton<ITrainingService, TrainingService>();
            this.container.Singleton<ILocalStorageAccess, LocalStorageAccess>();
            this.container.Singleton<IPhotoService, PhotoService>();
            this.container.Singleton<IWorkoutService, WorkoutService>();

            AddCustomConventions();
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = this.container.GetInstance(service, key);
            if (instance != null)
            {
                return instance;
            }

            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return this.container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            this.container.BuildUp(instance);
        }

        private static void AddCustomConventions()
        {
            ConventionManager.AddElementConvention<Pivot>(
                ItemsControl.ItemsSourceProperty,
                "SelectedItem",
                "SelectionChanged").ApplyBinding = (viewModelType, path, property, element, convention) =>
                {
                    if (ConventionManager.GetElementConvention(typeof(ItemsControl))
                        .ApplyBinding(viewModelType, path, property, element, convention))
                    {
                        ConventionManager.ConfigureSelectedItem(
                            element,
                            Pivot.SelectedItemProperty,
                            viewModelType,
                            path);
                        ConventionManager.ApplyHeaderTemplate(
                            element,
                            Pivot.HeaderTemplateProperty,
                            null,
                            viewModelType);
                        return true;
                    }

                    return false;
                };

            ConventionManager.AddElementConvention<Panorama>(
                ItemsControl.ItemsSourceProperty,
                "SelectedItem",
                "SelectionChanged").ApplyBinding = (viewModelType, path, property, element, convention) =>
                {
                    if (ConventionManager.GetElementConvention(typeof(ItemsControl))
                        .ApplyBinding(viewModelType, path, property, element, convention))
                    {
                        ConventionManager.ConfigureSelectedItem(
                            element,
                            Panorama.SelectedItemProperty,
                            viewModelType,
                            path);
                        ConventionManager.ApplyHeaderTemplate(
                            element,
                            Panorama.HeaderTemplateProperty,
                            null,
                            viewModelType);
                        return true;
                    }

                    return false;
                };

            ConventionManager.AddElementConvention<BindableAppBarButton>(
                Control.IsEnabledProperty,
                "DataContext",
                "Click");
            ConventionManager.AddElementConvention<BindableAppBarMenuItem>(
                Control.IsEnabledProperty,
                "DataContext",
                "Click");

            ConventionManager.AddElementConvention<ListPicker>(
                ItemsControl.ItemsSourceProperty,
                "SelectedItem",
                "SelectionChanged").ApplyBinding = (viewModelType, path, property, element, convention) =>
                {
                    if (ConventionManager.GetElementConvention(typeof(ItemsControl))
                        .ApplyBinding(viewModelType, path, property, element, convention))
                    {
                        ConventionManager.ConfigureSelectedItem(
                            element,
                            ListPicker.SelectedItemProperty,
                            viewModelType,
                            path);

                        return true;
                    }

                    return false;
                };
        }
    }
}