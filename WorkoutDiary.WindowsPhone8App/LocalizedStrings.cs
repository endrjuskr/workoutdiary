﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocalizedStrings.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp
{
    #region

    using WorkoutDiary.WindowsPhoneApp.Resources;

    #endregion

    /// <summary>
    ///     Provides access to string resources.
    /// </summary>
    public class LocalizedStrings
    {
        private static readonly AppResources _localizedResources = new AppResources();

        public AppResources LocalizedResources
        {
            get
            {
                return _localizedResources;
            }
        }
    }
}