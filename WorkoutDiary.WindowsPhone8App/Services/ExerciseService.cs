﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExerciseService.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;

    using WorkoutDiary.Core.Models;

    #endregion

    public class ExerciseService : IExerciseService
    {
        public async Task<Exercise> Get(string id)
        {
            try
            {
                return
                    await
                    App.MobileService.InvokeApiAsync<Exercise>(
                        "exercise/get",
                        HttpMethod.Get,
                        new Dictionary<string, string> { { "id", id } });
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<List<Exercise>> GetAll()
        {
            try
            {
                var list =
                    await
                    App.MobileService.InvokeApiAsync<ExerciseCollection>("exercise/getall", HttpMethod.Get, null);
                return list.Exercises.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<Exercise> Insert(Exercise exercise)
        {
            return await App.MobileService.InvokeApiAsync<Exercise, Exercise>("exercise/insert", exercise);
        }
    }
}