﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IExerciseService.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System.Collections.Generic;
    using System.Threading.Tasks;

    using WorkoutDiary.Core.Models;

    #endregion

    public interface IExerciseService
    {
        Task<Exercise> Get(string id);

        Task<List<Exercise>> GetAll();

        Task<Exercise> Insert(Exercise exercise);
    }
}