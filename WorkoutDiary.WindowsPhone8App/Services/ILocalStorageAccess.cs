﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILocalStorageAccess.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    public interface ILocalStorageAccess
    {
        #region Public Methods and Operators

        bool GetBooleanValue(string key);

        object GetInfoValue(string key);

        int GetIntValue(string key);

        void SaveInfo(string key, object value);

        void RemoveInfo(string key);

        #endregion
    }
}