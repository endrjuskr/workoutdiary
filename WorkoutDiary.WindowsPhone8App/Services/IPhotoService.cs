﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPhotoService.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System.IO;
    using System.Threading.Tasks;

    using WorkoutDiary.Core.Models;

    #endregion

    public interface IPhotoService
    {
        Task<Photo> Insert(Stream image);
    }
}