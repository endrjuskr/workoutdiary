﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITrainingService.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System.Collections.Generic;
    using System.Threading.Tasks;

    using WorkoutDiary.Core.Models;

    #endregion

    public interface ITrainingService
    {
        Task<Training> Get(string id);

        Task<List<Training>> GetAll();

        Task<Training> Insert(Training training);
    }
}