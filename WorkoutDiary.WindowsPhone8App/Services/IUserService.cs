﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserService.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System.Threading.Tasks;

    using Microsoft.WindowsAzure.MobileServices;

    #endregion

    public interface IUserService
    {
        Task<bool> Hello();

        Task<bool> SignIn(MobileServiceAuthenticationProvider provider);

        Task<bool> CheckSignIn(MobileServiceAuthenticationProvider provider);
    }
}