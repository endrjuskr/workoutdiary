﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkoutDiary.Core.Models;

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    public interface IWorkoutService
    {
        Task<Workout> GetWorkout(string id);

        Task<RestDay> GetRestDay(string id);

        Task<List<RestDay>> GetAll(string id);

        Task<Workout> Insert(Workout workout);

        Task<RestDay> Insert(RestDay restDay);
    }
}
