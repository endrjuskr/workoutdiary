﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocalStorageAccess.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System.IO.IsolatedStorage;

    #endregion

    public class LocalStorageAccess : ILocalStorageAccess
    {
        #region Public Methods and Operators

        public bool GetBooleanValue(string key)
        {
            var value = (string)this.GetInfoValue(key);
            return value != null && bool.Parse(value);
        }

        public object GetInfoValue(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                return IsolatedStorageSettings.ApplicationSettings[key];
            }

            return null;
        }

        public int GetIntValue(string key)
        {
            var value = (string)this.GetInfoValue(key);
            return value != null ? int.Parse(value) : 0;
        }

        public void SaveInfo(string key, object value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                IsolatedStorageSettings.ApplicationSettings[key] = value;
            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings.Add(key, value);
            }

            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public void RemoveInfo(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                IsolatedStorageSettings.ApplicationSettings.Remove(key);
            }
        }

        #endregion
    }
}