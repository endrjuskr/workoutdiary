﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PhotoService.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Threading.Tasks;

    using Microsoft.WindowsAzure.Storage.Auth;
    using Microsoft.WindowsAzure.Storage.Blob;

    using WorkoutDiary.Core.Models;

    #endregion

    public class PhotoService : IPhotoService
    {
        public async Task<Photo> Insert(Stream image)
        {
            var photo = new Photo { ContainerName = "todoitemimages", ResourceName = Guid.NewGuid() + ".jpg" };
            // Send the item to be inserted. When blob properties are set this
            // generates an SAS in the response.
            var returnPhoto =
                await
                App.MobileService.InvokeApiAsync<Photo, Photo>(
                    "photo/insert",
                    photo,
                    HttpMethod.Post,
                    new Dictionary<string, string>());

            // If we have a returned SAS, then upload the blob.
            if (!string.IsNullOrEmpty(returnPhoto.SasQueryString))
            {
                // Get the URI generated that contains the SAS 
                // and extract the storage credentials.
                StorageCredentials cred = new StorageCredentials(returnPhoto.SasQueryString);
                var imageUri = new Uri(returnPhoto.ImageUri);

                // Instantiate a Blob store container based on the info in the returned item.
                CloudBlobContainer container =
                    new CloudBlobContainer(
                        new Uri(string.Format("https://{0}/{1}", imageUri.Host, photo.ContainerName)),
                        cred);

                // Upload the new image as a BLOB from the stream.
                CloudBlockBlob blobFromSASCredential = container.GetBlockBlobReference(returnPhoto.ResourceName);
                await blobFromSASCredential.UploadFromStreamAsync(image);

                // When you request an SAS at the container-level instead of the blob-level,
                // you are able to upload multiple streams using the same container credentials.
            }

            return returnPhoto;
        }
    }
}