﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingService.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;

    using WorkoutDiary.Core.Models;

    #endregion

    public class TrainingService : ITrainingService
    {
        public async Task<Training> Get(string id)
        {
            try
            {
                return
                    await
                    App.MobileService.InvokeApiAsync<Training>(
                        "training/get",
                        HttpMethod.Get,
                        new Dictionary<string, string> { { "id", id } });
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<List<Training>> GetAll()
        {
            try
            {
                var list = await App.MobileService.InvokeApiAsync<TrainingCollection>("training/getall", HttpMethod.Get, null);
                return list.Trainings.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<Training> Insert(Training training)
        {
            return await App.MobileService.InvokeApiAsync<Training, Training>("training/insert", training);
        }
    }
}