﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserService.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    using LinqToTwitter;

    using Microsoft.WindowsAzure.MobileServices;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Telerik.Windows.Controls;

    using WorkoutDiary.Core.Models;

    using User = WorkoutDiary.Core.Models.User;

    #endregion

    public class UserService : IUserService
    {
        public const string AUTH_TOKEN = "AUTH_TOKEN";

        public const string USER_ID = "USER_ID";

        public const string USER_NAME = "USER_NAME";

        private readonly ILocalStorageAccess localStorageAccess;

        private MobileServiceUser user;

        public UserService(ILocalStorageAccess localStorageAccess)
        {
            this.localStorageAccess = localStorageAccess;
        }

        public async Task<bool> Hello()
        {
            try
            {
                await
                    App.MobileService.InvokeApiAsync<string>(
                        "user/hello",
                        HttpMethod.Get,
                        new Dictionary<string, string>());
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> SignIn(MobileServiceAuthenticationProvider provider)
        {
            byte[] entropy = { 1, 8, 3, 6, 5 };
            try
            {
                this.user = await App.MobileService.LoginAsync(provider);
                App.MobileService.CurrentUser = this.user;
                var userBytes = Encoding.Unicode.GetBytes(JsonConvert.SerializeObject(this.user));
                byte[] encryptedUser = ProtectedData.Protect(userBytes, entropy);
                this.localStorageAccess.SaveInfo(AUTH_TOKEN, encryptedUser);
                var userID = ulong.Parse(this.user.UserId.Split(':')[1]);
                await this.RegisterUser(userID, provider);
                return true;
            }
            catch (InvalidOperationException ex) {}

            return false;
        }

        public async Task<bool> CheckSignIn(MobileServiceAuthenticationProvider provider)
        {
            byte[] entropy = { 1, 8, 3, 6, 5 };

            if (this.localStorageAccess.GetInfoValue(AUTH_TOKEN) != null)
            {
                var encryptedUser = this.localStorageAccess.GetInfoValue(AUTH_TOKEN) as byte[];
                var userBytes = ProtectedData.Unprotect(encryptedUser, entropy);
                this.user =
                    JsonConvert.DeserializeObject<MobileServiceUser>(
                        Encoding.Unicode.GetString(userBytes, 0, userBytes.Length));

                App.MobileService.CurrentUser = this.user;

                var result = await this.Hello();
                if (result)
                {
                    return true;
                }

                this.localStorageAccess.RemoveInfo(AUTH_TOKEN);
                this.localStorageAccess.RemoveInfo(USER_ID);
                this.localStorageAccess.RemoveInfo(USER_NAME);
            }

            return false;
        }

        public async Task RegisterUser(ulong userId, MobileServiceAuthenticationProvider provider)
        {
            User b = await App.MobileService.InvokeApiAsync<User>("user/checkuser", HttpMethod.Get, new Dictionary<string, string>() {{ "authid", userId.ToString()}});
            var auth = new SingleUserAuthorizer
            {
                CredentialStore =
                    new SingleUserInMemoryCredentialStore
                    {
                        ConsumerKey
                            =
                            "yhx3MjIv0FhcYSF9oU3jaXOMs",
                        ConsumerSecret
                            =
                            "JzsNJZSmgvT7QwfPUpOghzcFl3nHzLj27EvNTkcEeygTG7D84m",
                        AccessToken
                            =
                            "279500033-T5hWD5JePawqCwYOXQzqt21d9iBgAuMUIIL9NErL",
                        AccessTokenSecret
                            =
                            "ufkT39VPFwMvLZOBqr5nRAva8WhpEvUqZ521YnUGK24sV"
                    }
            };
            var tt = new TwitterContext(auth);
            var user =
                await
                (from tweet in tt.User
                 where tweet.Type == UserType.Show && tweet.UserID == userId
                 select tweet).SingleOrDefaultAsync();
            if (b == null)
            {
                var yearInput =
                    await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide birth year");
                var heightInput =
                    await
                    RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide your height");
                var weightInput =
                    await
                    RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide your weight");
                switch (provider)
                {
                    case MobileServiceAuthenticationProvider.Twitter:
                       
                        var newUser = new User
                                      {
                                          AuthProvider = AuthProvider.Twitter,
                                          Name = user.Name,
                                          AuthID = userId,
                                          Photo = user.ProfileImageUrl,
                                          Weight = uint.Parse(weightInput.Text),
                                          Height = uint.Parse(heightInput.Text),
                                          BirthYear = uint.Parse(yearInput.Text)
                                      };
                        try
                        {
                            b =
                                await
                                App.MobileService.InvokeApiAsync<User, User>(
                                    "user/insert",
                                    newUser);
                        }
                        catch (Exception ex)
                        {
                            // ignored
                        }

                        break;
                }
            }

            this.localStorageAccess.SaveInfo(USER_ID, b.ID);
            this.localStorageAccess.SaveInfo(USER_NAME, b.Name);
        }
    }
}