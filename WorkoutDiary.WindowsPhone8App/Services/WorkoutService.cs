﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WorkoutDiary.Core.Models;

namespace WorkoutDiary.WindowsPhoneApp.Services
{
    public class WorkoutService : IWorkoutService
    {
        public async Task<Workout> GetWorkout(string id)
        {
            try
            {
                return
                    await
                    App.MobileService.InvokeApiAsync<Workout>(
                        "workout/getworkout",
                        HttpMethod.Get,
                        new Dictionary<string, string> { { "id", id } });
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<RestDay> GetRestDay(string id)
        {
            try
            {
                return
                    await
                    App.MobileService.InvokeApiAsync<RestDay>(
                        "workout/getrestday",
                        HttpMethod.Get,
                        new Dictionary<string, string> { { "id", id } });
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<List<RestDay>> GetAll(string id)
        {
            try
            {
                var list =
                    await
                    App.MobileService.InvokeApiAsync<WorkoutCollection>("workout/getall", HttpMethod.Get, new Dictionary<string, string>() { { "id", id }});
                return list.Workouts.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<Workout> Insert(Workout workout)
        {
            return await App.MobileService.InvokeApiAsync<Workout, Workout>("workout/insertworkout", workout);
        }

        public async Task<RestDay> Insert(RestDay restDay)
        {
            return await App.MobileService.InvokeApiAsync<RestDay, RestDay>("workout/insertrestday", restDay);
        }
    }
}
