﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataTemplateSelector.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Utils
{
    #region

    using System.Windows;
    using System.Windows.Controls;

    #endregion

    public abstract class DataTemplateSelector : ContentControl
    {
        #region Public Methods and Operators

        public virtual DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return null;
        }

        #endregion

        #region Methods

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            this.ContentTemplate = this.SelectTemplate(newContent, this);
        }

        #endregion
    }
}