﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InvertedBooleanToVisibilityConverter.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Utils
{
    #region

    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    #endregion

    public class InvertedBooleanToVisibilityConverter : IValueConverter
    {
        #region Public Methods and Operators

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var boolValue = (bool)value;

            return !boolValue ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}