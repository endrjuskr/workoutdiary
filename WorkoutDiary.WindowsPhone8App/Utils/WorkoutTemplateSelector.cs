﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutTemplateSelector.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.Utils
{
    #region

    using System.Windows;

    using WorkoutDiary.Core.Models;

    #endregion

    public class WorkoutTemplateSelector : DataTemplateSelector
    {
        #region Public Methods and Operators

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var day = item as RestDay;
            if (day != null)
            {
                return day.IsRestDay ? this.RestDay : this.Workout;
            }

            return base.SelectTemplate(item, container);
        }

        #endregion

        #region Public Properties

        public DataTemplate Workout { get; set; }

        public DataTemplate RestDay { get; set; }

        #endregion
    }
}