﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExerciseDetailViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise
{
    #region

    using System.Collections.ObjectModel;

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class ExerciseDetailViewModel : ViewModelBase
    {
        private ObservableCollection<BodyPartElem> bodyParts;

        private ObservableCollection<DifficultLevelElem> difficultyLevels;

        private bool isCreate;

        private Exercise model;

        private BodyPartElem selectedBodyPart;

        private DifficultLevelElem selectedDifficultyLevel;

        public ExerciseDetailViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess) {}

        public ObservableCollection<DifficultLevelElem> DifficultyLevels
        {
            get
            {
                return this.difficultyLevels;
            }
            set
            {
                this.difficultyLevels = value;
                this.NotifyOfPropertyChange(() => this.DifficultyLevels);
            }
        }

        public string Name
        {
            get
            {
                return (this.model != null) ? this.model.Name : string.Empty;
            }
            set
            {
                this.model.Name = value;
                NotifyOfPropertyChange(() => this.Name);
            }
        }

        public string Description
        {
            get
            {
                return (this.model != null) ? this.model.Description : string.Empty;
            }
            set
            {
                this.model.Description = value;
                NotifyOfPropertyChange(() => this.Description);
            }
        }

        public ObservableCollection<BodyPartElem> BodyParts
        {
            get
            {
                return this.bodyParts;
            }
            set
            {
                this.bodyParts = value;
                this.NotifyOfPropertyChange(() => this.BodyParts);
            }
        }

        public DifficultLevelElem SelectedDifficultyLevel
        {
            get
            {
                return this.selectedDifficultyLevel;
            }
            set
            {
                this.selectedDifficultyLevel = value;
                this.NotifyOfPropertyChange(() => this.SelectedDifficultyLevel);
            }
        }

        public BodyPartElem SelectedBodyPart
        {
            get
            {
                return this.selectedBodyPart;
            }
            set
            {
                this.selectedBodyPart = value;
                this.NotifyOfPropertyChange(() => this.SelectedBodyPart);
            }
        }

        public bool IsCreate
        {
            get
            {
                return this.isCreate;
            }
            set
            {
                this.isCreate = value;
                this.NotifyOfPropertyChange(() => this.IsCreate);
            }
        }

        public Exercise Model
        {
            get
            {
                return this.model;
            }
            set
            {
                this.model = value;
                this.NotifyOfPropertyChange(() => this.Model);
                NotifyOfPropertyChange(() => this.Name);
                NotifyOfPropertyChange(() => this.Description);
            }
        }
    }

    public class BodyPartElem
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }

    public class DifficultLevelElem
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}