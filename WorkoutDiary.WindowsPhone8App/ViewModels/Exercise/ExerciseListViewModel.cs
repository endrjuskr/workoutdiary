﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExerciseListViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise
{
    #region

    using System.Collections.Generic;

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class ExerciseListViewModel : ViewModelBase
    {
        private List<Exercise> items;

        public ExerciseListViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess,
            IExerciseService exerciseService)
            : base(navigationService, userService, localStorageAccess)
        {
            this.LoadingData = false;
            this.ExerciseService = exerciseService;
        }

        public IExerciseService ExerciseService { get; private set; }

        public List<Exercise> Items
        {
            get
            {
                return this.items;
            }
            set
            {
                this.items = value;
                this.NotifyOfPropertyChange(() => this.Items);
            }
        }

        protected override async void OnActivate()
        {
            base.OnActivate();
            this.LoadingData = true;
            this.Items = new List<Exercise>();
            this.Items = await this.ExerciseService.GetAll();
            this.LoadingData = false;
        }

        public void ExerciseSelected(object obj)
        {
            var exercise = (Exercise)obj;
            this.NavigationService.UriFor<ExerciseViewModel>()
                .WithParam(x => x.ModelID, exercise.ID)
                .WithParam(x => x.IsCreate, false)
                .Navigate();
        }

        public void ExerciseAdd()
        {
            this.NavigationService.UriFor<ExerciseViewModel>()
                .WithParam(x => x.ModelID, "")
                .WithParam(x => x.IsCreate, true)
                .Navigate();
        }
    }
}