﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExercisePhotoViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise
{
    #region

    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class ExercisePhotoViewModel : ViewModelBase
    {
        private List<Photo> photos;

        public List<Photo> Photos
        {
            get
            {
                return this.photos;
            }
            set
            {
                this.photos = value;
                NotifyOfPropertyChange(() => this.Photos);
            }
        }

        public ExercisePhotoViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess) {}
    }
}