﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExerciseViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reactive.Linq;

    using Caliburn.Micro;

    using LinqToTwitter;

    using Microsoft.Phone.Shell;
    using Microsoft.Phone.Tasks;

    using Telerik.Windows.Controls;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class ExerciseViewModel : PivotViewModelBase
    {
        private readonly ExerciseDetailViewModel detailViewModel;

        private readonly IExerciseService exerciseService;

        private readonly IPhotoService photoService;

        private readonly ExercisePhotoViewModel photoViewModel;

        private bool isCreate;

        private Exercise model;

        public ExerciseViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess,
            ExerciseDetailViewModel exerciseDetailViewModel,
            ExercisePhotoViewModel exercisePhotoViewModel,
            IPhotoService photoService,
            IExerciseService exerciseService)
            : base(navigationService, userService, localStorageAccess)
        {
            this.detailViewModel = exerciseDetailViewModel;
            this.photoViewModel = exercisePhotoViewModel;
            this.photoService = photoService;
            this.exerciseService = exerciseService;
        }

        public Exercise Model
        {
            get
            {
                return this.model;
            }
            set
            {
                this.model = value;
                this.photoViewModel.Photos = new List<Photo>(value.Photos);
                this.detailViewModel.Model = value;
                var dl = Enum.GetValues(typeof(DifficultyLevel));
                var dll = (from object v in dl select v.ToString()).ToList();
                this.detailViewModel.DifficultyLevels = new ObservableCollection<DifficultLevelElem>(dll.Select(x => new DifficultLevelElem() { Name = x }));
                var bp = Enum.GetValues(typeof(BodyPart));
                var bpl = (from object v in bp select v.ToString()).ToList();
                this.detailViewModel.BodyParts = new ObservableCollection<BodyPartElem>(bpl.Select(x => new BodyPartElem() { Name = x }));
                this.detailViewModel.SelectedBodyPart = this.detailViewModel.BodyParts.Last(x => x.Name == value.BodyPart.ToString());
                this.detailViewModel.SelectedDifficultyLevel = this.detailViewModel.DifficultyLevels.Last(x => x.Name == value.DifficultyLevel.ToString());
                this.NotifyOfPropertyChange(() => this.Model);
            }
        }

        public string ModelID
        {
            get
            {
                return "";
            }
            set
            {
                if (value != string.Empty)
                {
                    this.SetExercise(value);
                }
                else
                {
                    var e = new Exercise();
                    e.Photos = new List<Photo>();
                    this.Model = e;
                }
            }
        }

        public async void SetExercise(string id)
        {
            this.Model = await this.exerciseService.Get(id);
        }

        public bool IsCreate
        {
            get
            {
                return this.isCreate;
            }
            set
            {
                this.isCreate = value;
                this.detailViewModel.IsCreate = value;
                this.NotifyOfPropertyChange(() => this.IsCreate);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            this.detailViewModel.DisplayName = "Details";
            this.photoViewModel.DisplayName = "Photos";
            this.Items.Add(this.detailViewModel);
            this.Items.Add(this.photoViewModel);

            this.ActivateItem(this.detailViewModel);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            if (PhoneApplicationService.Current.State.ContainsKey("exercise"))
            {
                this.Model = (Exercise)PhoneApplicationService.Current.State["exercise"];
                this.IsCreate = true;
                PhoneApplicationService.Current.State.Remove("exercise");
            }
        }

        public void AddPhoto()
        {
            this.model.Photos = this.photoViewModel.Photos.ToList();
            this.model.DifficultyLevel = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel), this.detailViewModel.SelectedDifficultyLevel.Name);
            this.model.BodyPart = (BodyPart)Enum.Parse(typeof(BodyPart), this.detailViewModel.SelectedBodyPart.Name);
            PhoneApplicationService.Current.State["exercise"] = this.model;
            var cameraCaptureTask = new CameraCaptureTask();
            cameraCaptureTask.Completed += this.cameraCaptureTask_Completed;
            cameraCaptureTask.Show();
        }

        public void Save()
        {
            this.model.Photos = this.photoViewModel.Photos.ToList();
            this.model.DifficultyLevel = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel), this.detailViewModel.SelectedDifficultyLevel.Name);
            this.model.BodyPart = (BodyPart)Enum.Parse(typeof(BodyPart), this.detailViewModel.SelectedBodyPart.Name);
            this.exerciseService.Insert(this.model);
            this.NavigationService.GoBack();
        }

        private async void cameraCaptureTask_Completed(object sender, PhotoResult e)
        {
            var titleInput =
                await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide photo title");
            var photo = await this.photoService.Insert(e.ChosenPhoto);
            photo.Title = titleInput.Text;
            var t = new List<Photo>();
            foreach (var ph in this.photoViewModel.Photos)
            {
                t.Add(ph);
            }
            t.Add(photo);

            this.photoViewModel.Photos = t;
        }
    }
}