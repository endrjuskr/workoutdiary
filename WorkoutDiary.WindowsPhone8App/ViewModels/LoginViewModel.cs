﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoginViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels
{
    #region

    using System.Windows;

    using Caliburn.Micro;

    using Microsoft.WindowsAzure.MobileServices;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class LoginViewModel : ViewModelBase
    {
        public LoginViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess)
        {
            this.LoadingData = false;
        }

        public async void TwitterSignIn()
        {
            this.LoadingData = true;
            var result = await this.UserService.SignIn(MobileServiceAuthenticationProvider.Twitter);
            this.LoadingData = false;
            if (!result)
            {
                this.HandleLoginError();
            }
            else
            {
                this.NavigationService.UriFor<MenuViewModel>().Navigate();
            }
        }

        public async void GoogleSignIn()
        {
            var result = await this.UserService.SignIn(MobileServiceAuthenticationProvider.Google);
            if (!result)
            {
                this.HandleLoginError();
            }
        }

        protected override async void OnActivate()
        {
            this.LoadingData = true;
            var result = await this.UserService.CheckSignIn(MobileServiceAuthenticationProvider.Twitter);
            if (result)
            {
                this.NavigationService.UriFor<MenuViewModel>().Navigate();
            }
            this.LoadingData = false;
        }

        private async void HandleLoginError()
        {
            MessageBox.Show("Error occured. Please try later.", "Error", MessageBoxButton.OK);
        }
    }
}