﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MenuViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Training;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Workout;

    #endregion

    public class MenuViewModel : ViewModelBase
    {

        public string Title { get; set; }

        public MenuViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess)
        {
            this.LoadingData = false;
        }

        public void Logout()
        {
            this.LocalStorageAccess.RemoveInfo(Services.UserService.AUTH_TOKEN);
            this.LocalStorageAccess.RemoveInfo(Services.UserService.USER_ID);
            this.NavigationService.UriFor<LoginViewModel>().Navigate();
        }

        public void Exercises()
        {
            this.NavigationService.UriFor<ExerciseListViewModel>().Navigate();
        }

        public void Trainings()
        {
            this.NavigationService.UriFor<TrainingListViewModel>().Navigate();
        }

        public void PastWorkouts()
        {
            this.NavigationService.UriFor<WorkoutListViewModel>().Navigate();
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            this.Title = "Hello, " + this.LocalStorageAccess.GetInfoValue(Services.UserService.USER_NAME);
            this.NotifyOfPropertyChange(() => this.Title);
        }

        public void AddRestDay()
        {
            this.NavigationService.UriFor<RestDayViewModel>().
                WithParam(x => x.ModelID, "").WithParam(x => x.IsCreate, true).Navigate();
        }

        public void AddWorkout()
        {
            this.NavigationService.UriFor<WorkoutViewModel>().
                WithParam(x => x.ModelID, "").WithParam(x => x.IsCreate, true).Navigate();
        }
    }
}