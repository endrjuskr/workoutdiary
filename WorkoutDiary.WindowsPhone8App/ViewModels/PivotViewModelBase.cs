﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PivotViewModelBase.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public abstract class PivotViewModelBase : Conductor<IScreen>.Collection.OneActive
    {
        #region Constructors and Destructors

        protected PivotViewModelBase(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
        {
            this.NavigationService = navigationService;
            this.UserService = userService;
            this.LocalStorageAccess = localStorageAccess;
        }

        public IUserService UserService { get; private set; }

        public INavigationService NavigationService { get; private set; }

        public ILocalStorageAccess LocalStorageAccess { get; private set; }

        #endregion
    }
}