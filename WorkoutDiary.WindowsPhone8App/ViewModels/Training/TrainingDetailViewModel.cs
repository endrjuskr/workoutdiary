﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingDetailViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Training
{
    #region

    using System.Collections.ObjectModel;

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise;

    #endregion

    public class TrainingDetailViewModel : ViewModelBase
    {
        private ObservableCollection<DifficultLevelElem> difficultyLevels;

        private bool isCreate;

        private Training model;

        private DifficultLevelElem selectedDifficultyLevel;

        public TrainingDetailViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess) {}

        public ObservableCollection<DifficultLevelElem> DifficultyLevels
        {
            get
            {
                return this.difficultyLevels;
            }
            set
            {
                this.difficultyLevels = value;
                NotifyOfPropertyChange(() => this.DifficultyLevels);
            }
        }

        public DifficultLevelElem SelectedDifficultyLevel
        {
            get
            {
                return this.selectedDifficultyLevel;
            }
            set
            {
                this.selectedDifficultyLevel = value;
                NotifyOfPropertyChange(() => this.SelectedDifficultyLevel);
            }
        }

        public bool IsCreate
        {
            get
            {
                return this.isCreate;
            }
            set
            {
                this.isCreate = value;
                NotifyOfPropertyChange(() => this.IsCreate);
            }
        }

        public string Name
        {
            get
            {
                return (this.model != null) ? this.model.Name : string.Empty;
            }
            set
            {
                this.model.Name = value;
                NotifyOfPropertyChange(() => this.Name);
            }
        }

        public string Description
        {
            get
            {
                return (this.model != null) ? this.model.Description : string.Empty;
            }
            set
            {
                this.model.Description = value;
                NotifyOfPropertyChange(() => this.Description);
            }
        }

        public uint Duration
        {
            get
            {
                return (this.model != null) ? this.model.Duration : 0;
            }
            set
            {
                this.model.Duration = value;
                NotifyOfPropertyChange(() => this.Duration);
            }
        }

        public Training Model
        {
            get
            {
                return this.model;
            }
            set
            {
                this.model = value;
                NotifyOfPropertyChange(() => Model);
                NotifyOfPropertyChange(() => Duration);
                NotifyOfPropertyChange(() => Name);
                NotifyOfPropertyChange(() => Description);
            }
        }
    }
}