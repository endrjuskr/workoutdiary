﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingExerciseViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Training
{
    using System.Collections.Generic;

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;

    public class TrainingExerciseViewModel : ViewModelBase
    {
        private List<TrainingPart> trainingParts;

        public List<TrainingPart> TrainingParts
        {
            get
            {
                return this.trainingParts;
            }
            set
            {
                this.trainingParts = value;
                NotifyOfPropertyChange(() => this.TrainingParts);
            }
        }

        public TrainingExerciseViewModel(INavigationService navigationService, IUserService userService, ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess) {}
    }
}