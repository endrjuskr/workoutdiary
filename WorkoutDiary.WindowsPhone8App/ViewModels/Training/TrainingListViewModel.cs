﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingListViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Training
{
    #region

    using System.Collections.Generic;

    using Caliburn.Micro;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class TrainingListViewModel : ViewModelBase
    {
        private List<Core.Models.Training> items;

        public TrainingListViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess,
            ITrainingService trainingService)
            : base(navigationService, userService, localStorageAccess)
        {
            this.LoadingData = false;
            this.TrainingService = trainingService;
        }

        public ITrainingService TrainingService { get; private set; }

        public List<Core.Models.Training> Items
        {
            get
            {
                return this.items;
            }
            set
            {
                this.items = value;
                this.NotifyOfPropertyChange(() => this.Items);
            }
        }

        protected override async void OnActivate()
        {
            base.OnActivate();
            this.LoadingData = true;
            this.Items = new List<Core.Models.Training>();
            this.Items = await this.TrainingService.GetAll();
            this.LoadingData = false;
        }

        public void TrainingSelected(object obj)
        {
            var training = (Core.Models.Training)obj;
            this.NavigationService.UriFor<TrainingViewModel>()
                .WithParam(x => x.ModelID, training.ID)
                .WithParam(x => x.IsCreate, false)
                .Navigate();
        }

        public void TrainingAdd()
        {
            this.NavigationService.UriFor<TrainingViewModel>()
                .WithParam(x => x.ModelID, "")
                .WithParam(x => x.IsCreate, true)
                .Navigate();
        }
    }
}