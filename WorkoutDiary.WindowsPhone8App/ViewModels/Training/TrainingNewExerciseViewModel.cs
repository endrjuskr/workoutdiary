﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingNewExerciseViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Microsoft.Phone.Shell;
using Telerik.Windows.Controls;
using WorkoutDiary.Core.Models;
using WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Training
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class TrainingNewExerciseViewModel : ViewModelBase
    {
        private List<Core.Models.Exercise> items;

        private Core.Models.Training model;

        public TrainingNewExerciseViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess,
            IExerciseService exerciseService)
            : base(navigationService, userService, localStorageAccess)
        {
            this.LoadingData = false;
            this.ExerciseService = exerciseService;
        }

        public IExerciseService ExerciseService { get; private set; }

        public List<Core.Models.Exercise> Items
        {
            get
            {
                return this.items;
            }
            set
            {
                this.items = value;
                this.NotifyOfPropertyChange(() => this.Items);
            }
        }

        protected override async void OnActivate()
        {
            base.OnActivate();
            this.LoadingData = true;
            this.Items = new List<Core.Models.Exercise>();
            this.Items = await this.ExerciseService.GetAll();
            this.LoadingData = false;
            this.model = (Core.Models.Training)PhoneApplicationService.Current.State["training"];
        }

        public async void ExerciseSelected(object obj)
        {
            var exercise = (Core.Models.Exercise)obj;
            var seriesInput =
                await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide series number");
            var breakInput =
                await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide break");

            var part = new TrainingPart()
            {
                Break = Convert.ToDouble(breakInput.Text),
                Series = Convert.ToUInt32(seriesInput.Text),
                Exercise = exercise,
                ExerciseID = exercise.ID
            };

            model.Parts.Add(part);

            PhoneApplicationService.Current.State["training"] = model;

            this.NavigationService.GoBack();
        }
    }
}