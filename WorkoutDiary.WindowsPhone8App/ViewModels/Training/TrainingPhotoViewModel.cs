﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingPhotoViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Training
{
    #region

    using System.Collections.Generic;

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class TrainingPhotoViewModel : ViewModelBase
    {
        private List<Photo> photos;

        public List<Photo> Photos
        {
            get
            {
                return this.photos;
            }
            set
            {
                this.photos = value;
                NotifyOfPropertyChange(() => this.Photos);
            }
        }


        public TrainingPhotoViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess) {}
    }
}