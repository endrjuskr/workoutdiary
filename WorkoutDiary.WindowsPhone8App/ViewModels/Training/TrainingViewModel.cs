﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainingViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.Phone.Shell;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Training
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using Caliburn.Micro;

    using LinqToTwitter;

    using Microsoft.Phone.Tasks;

    using Telerik.Windows.Controls;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Exercise;

    #endregion

    public class TrainingViewModel : PivotViewModelBase
    {
        private readonly TrainingDetailViewModel detailViewModel;

        private readonly TrainingExerciseViewModel exerciseViewModel;

        private readonly IPhotoService photoService;

        private readonly TrainingPhotoViewModel photoViewModel;

        private readonly ITrainingService trainingService;

        private IExerciseService exerciseService;

        private bool isCreate;

        private Training model;

        public TrainingViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess,
            TrainingDetailViewModel trainingDetailViewModel,
            TrainingPhotoViewModel trainingPhotoViewModel,
            TrainingExerciseViewModel trainingExerciseViewModel,
            IPhotoService photoService,
            IExerciseService exerciseService,
            ITrainingService trainingService)
            : base(navigationService, userService, localStorageAccess)
        {
            this.detailViewModel = trainingDetailViewModel;
            this.photoViewModel = trainingPhotoViewModel;
            this.exerciseViewModel = trainingExerciseViewModel;
            this.photoService = photoService;
            this.exerciseService = exerciseService;
            this.trainingService = trainingService;
        }

        public string ModelID
        {
            get
            {
                return "";
            }
            set
            {
                if (value != string.Empty)
                {
                    this.SetTraining(value);
                }
                else
                {
                    var e = new Training();
                    e.Photos = new List<Photo>();
                    e.Parts = new List<TrainingPart>();
                    this.Model = e;
                }
            }
        }

        public async void SetTraining(string id)
        {
            var m = await this.trainingService.Get(id);
            foreach (var part in m.Parts)
            {
                part.Exercise = await this.exerciseService.Get(part.ExerciseID);
            }
            this.Model = m;
        }

        public Training Model
        {
            get
            {
                return this.model;
            }
            set
            {
                this.model = value;
                this.photoViewModel.Photos = new List<Photo>(value.Photos);
                this.exerciseViewModel.TrainingParts = new List<TrainingPart>(value.Parts);
                this.detailViewModel.Model = value;
                var dl = Enum.GetValues(typeof(DifficultyLevel));
                var dll = (from object v in dl select v.ToString()).ToList();
                this.detailViewModel.DifficultyLevels = new ObservableCollection<DifficultLevelElem>(dll.Select(x => new DifficultLevelElem() { Name = x }));
                this.detailViewModel.SelectedDifficultyLevel = this.detailViewModel.DifficultyLevels.Last(x => x.Name == value.DifficultyLevel.ToString());
                NotifyOfPropertyChange(() => this.Model);
            }
        }

        public bool IsCreate
        {
            get
            {
                return this.isCreate;
            }
            set
            {
                this.isCreate = value;
                this.detailViewModel.IsCreate = value;
                NotifyOfPropertyChange(() => this.IsCreate);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            this.detailViewModel.DisplayName = "Details";
            this.photoViewModel.DisplayName = "Photos";
            this.exerciseViewModel.DisplayName = "Exercises";
            this.Items.Add(this.detailViewModel);
            this.Items.Add(this.photoViewModel);
            this.Items.Add(this.exerciseViewModel);

            this.ActivateItem(this.detailViewModel);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            if (PhoneApplicationService.Current.State.ContainsKey("training"))
            {
                this.Model = (Training)PhoneApplicationService.Current.State["training"];
                this.IsCreate = true;
                PhoneApplicationService.Current.State.Remove("training");
            }
        }

        public void AddPhoto()
        {
            this.model.Photos = this.photoViewModel.Photos.ToList();
            this.model.Parts = this.exerciseViewModel.TrainingParts.ToList();
            this.model.DifficultyLevel = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel), this.detailViewModel.SelectedDifficultyLevel.Name);
            PhoneApplicationService.Current.State["training"] = this.model;
            var cameraCaptureTask = new CameraCaptureTask();
            cameraCaptureTask.Completed += cameraCaptureTask_Completed;
            cameraCaptureTask.Show();
        }

        public void AddExercise()
        {
            this.model.Photos = this.photoViewModel.Photos.ToList();
            this.model.Parts = this.exerciseViewModel.TrainingParts.ToList();
            this.model.DifficultyLevel = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel), this.detailViewModel.SelectedDifficultyLevel.Name);
            PhoneApplicationService.Current.State["training"] = this.model;
            this.NavigationService.UriFor<TrainingNewExerciseViewModel>().Navigate();
        }

        public void Save()
        {
            this.model.Photos = this.photoViewModel.Photos.ToList();
            this.model.Parts = this.exerciseViewModel.TrainingParts.ToList();
            this.model.DifficultyLevel = (DifficultyLevel) Enum.Parse(typeof(DifficultyLevel), this.detailViewModel.SelectedDifficultyLevel.Name);
            this.trainingService.Insert(this.model);
            this.NavigationService.GoBack();
        }

        private async void cameraCaptureTask_Completed(object sender, PhotoResult e)
        {
            var titleInput =
                await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide photo title");
            var photo = await this.photoService.Insert(e.ChosenPhoto);
            photo.Title = titleInput.Text;
            var t = new List<Photo>();
            foreach (var ph in this.photoViewModel.Photos)
            {
                t.Add(ph);
            }
            t.Add(photo);

            this.photoViewModel.Photos = t;
        }
    }
}