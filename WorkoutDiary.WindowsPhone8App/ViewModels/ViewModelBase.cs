﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewModelBase.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

namespace WorkoutDiary.WindowsPhoneApp.ViewModels
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public abstract class ViewModelBase : Screen
    {
        private bool loadingData;

        protected ViewModelBase(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
        {
            this.NavigationService = navigationService;
            this.UserService = userService;
            this.LocalStorageAccess = localStorageAccess;
        }

        public IUserService UserService { get; private set; }

        public INavigationService NavigationService { get; private set; }

        public ILocalStorageAccess LocalStorageAccess { get; private set; }

        public bool LoadingData
        {
            get
            {
                return this.loadingData;
            }
            set
            {
                this.loadingData = value;
                this.NotifyOfPropertyChange(() => this.LoadingData);
            }
        }
    }
}