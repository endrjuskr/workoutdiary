﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutDetailViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Workout
{
    #region

    using System.Collections.ObjectModel;

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class WorkoutDetailViewModel : ViewModelBase
    {
       private ObservableCollection<RatingElem> _ratings;

        private bool isCreate;

        private RestDay model;

        private RatingElem _selectedRating;

        public WorkoutDetailViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess) {}

        public ObservableCollection<RatingElem> Ratings
        {
            get
            {
                return this._ratings;
            }
            set
            {
                this._ratings = value;
                NotifyOfPropertyChange(() => this.Ratings);
            }
        }

        public RatingElem SelectedRating
        {
            get
            {
                return this._selectedRating;
            }
            set
            {
                this._selectedRating = value;
                NotifyOfPropertyChange(() => this.SelectedRating);
            }
        }

        public bool IsCreate
        {
            get
            {
                return this.isCreate;
            }
            set
            {
                this.isCreate = value;
                NotifyOfPropertyChange(() => this.IsCreate);
            }
        }

        public string DateTimeStr
        {
            get
            {
                return this.model == null ? string.Empty : this.model.DateTime.ToString();
            }
            set
            {
                this.model.DateTime = Convert.ToDateTime(value);
                NotifyOfPropertyChange(() => DateTimeStr);
            }
        }

        public RestDay Model
        {
            get
            {
                return this.model;
            }
            set
            {
                this.model = value;
                NotifyOfPropertyChange(() => this.Model);
                NotifyOfPropertyChange(() => DateTimeStr);
            }
        }
    }

    public class RatingElem
    {
        public int Value { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}