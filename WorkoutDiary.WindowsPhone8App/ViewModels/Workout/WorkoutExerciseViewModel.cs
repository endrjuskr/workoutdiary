﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutExerciseViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using WorkoutDiary.Core.Models;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Workout
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class WorkoutExerciseViewModel : ViewModelBase
    {
        private List<WorkoutExercise> exercises;

        public List<WorkoutExercise> Exercises
        {
            get
            {
                return this.exercises;
            }
            set
            {
                this.exercises = value;
                NotifyOfPropertyChange(() => this.Exercises);
            }
        }

        public WorkoutExerciseViewModel(INavigationService navigationService, IUserService userService, ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess) {}
    }
}