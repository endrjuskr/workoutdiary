﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutListViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using WorkoutDiary.WindowsPhoneApp.ViewModels.Training;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Workout
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class WorkoutListViewModel : ViewModelBase
    {
        private List<Core.Models.RestDay> items;

        public WorkoutListViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess,
            IWorkoutService workoutService)
            : base(navigationService, userService, localStorageAccess)
        {
            this.LoadingData = false;
            this.WorkoutService = workoutService;
        }

        public IWorkoutService WorkoutService { get; private set; }

        public List<Core.Models.RestDay> Items
        {
            get
            {
                return this.items;
            }
            set
            {
                this.items = value;
                this.NotifyOfPropertyChange(() => this.Items);
            }
        }

        protected override async void OnActivate()
        {
            base.OnActivate();
            this.LoadingData = true;
            this.Items = new List<Core.Models.RestDay>();
            var id = this.LocalStorageAccess.GetInfoValue(Services.UserService.USER_ID) as string;
            this.Items = await this.WorkoutService.GetAll(id);
            this.LoadingData = false;
        }

        public void WorkoutSelected(object obj)
        {
            var workout = (Core.Models.RestDay)obj;
            this.NavigationService.UriFor<WorkoutViewModel>()
                .WithParam(x => x.ModelID, workout.ID)
                .WithParam(x => x.IsCreate, false)
                .Navigate();
        }

        public void RestDaySelected(object obj)
        {
            var workout = (Core.Models.RestDay)obj;
            this.NavigationService.UriFor<RestDayViewModel>()
                .WithParam(x => x.ModelID, workout.ID)
                .WithParam(x => x.IsCreate, false)
                .Navigate();
        }
    }
}