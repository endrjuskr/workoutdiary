﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutMeasurmentViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using WorkoutDiary.Core.Models;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Workout
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class WorkoutMeasurmentViewModel : ViewModelBase
    {
        private List<Measurment> measurments;

        public List<Measurment> Measurments
        {
            get { return this.measurments; }
            set
            {
                this.measurments = value;
                NotifyOfPropertyChange(() => this.Measurments);
            }
        }

        public WorkoutMeasurmentViewModel(INavigationService navigationService, IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess)
        {
        }
    }
}