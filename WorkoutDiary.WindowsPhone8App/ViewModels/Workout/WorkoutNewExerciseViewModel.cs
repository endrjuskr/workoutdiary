﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutNewExerciseViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Phone.Shell;
using Telerik.Windows.Controls;
using WorkoutDiary.Core.Models;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Workout
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class WorkoutNewExerciseViewModel : ViewModelBase
    {
       private List<Core.Models.Exercise> items;

        private Core.Models.Workout model;

        public WorkoutNewExerciseViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess,
            IExerciseService exerciseService)
            : base(navigationService, userService, localStorageAccess)
        {
            this.LoadingData = false;
            this.ExerciseService = exerciseService;
        }

        public IExerciseService ExerciseService { get; private set; }

        public List<Core.Models.Exercise> Items
        {
            get
            {
                return this.items;
            }
            set
            {
                this.items = value;
                this.NotifyOfPropertyChange(() => this.Items);
            }
        }

        protected override async void OnActivate()
        {
            base.OnActivate();
            this.LoadingData = true;
            this.Items = new List<Core.Models.Exercise>();
            this.Items = await this.ExerciseService.GetAll();
            this.LoadingData = false;
            this.model = (Core.Models.Workout)PhoneApplicationService.Current.State["workout"];
        }

        public async void ExerciseSelected(object obj)
        {
            var exercise = (Core.Models.Exercise)obj;
            var seriesInput =
                await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide series number");

            int serC = Convert.ToInt32(seriesInput.Text);
            var s = new List<int>();
            for (int i = 0; i < serC; i++)
            {
                var seriescount =
                await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide count");
                s.Add(Convert.ToInt32(seriescount.Text));
            }

            var part = new WorkoutExercise()
            {
                Series = s.Select(x => new WorkoutSerie() {Count = x}).ToList(),
                ExerciseID = exercise.ID,
                Exercise = exercise
            };

            model.Exercises.Add(part);

            PhoneApplicationService.Current.State["workout"] = model;

            this.NavigationService.GoBack();
        }
    }
}