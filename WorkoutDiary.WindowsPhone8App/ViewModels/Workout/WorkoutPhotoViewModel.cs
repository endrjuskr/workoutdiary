﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutPhotoViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Workout
{
    #region

    using Caliburn.Micro;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;

    #endregion

    public class WorkoutPhotoViewModel : ViewModelBase
    {
        private List<Photo> photos;

        public List<Photo> Photos
        {
            get
            {
                return this.photos;
            }
            set
            {
                this.photos = value;
                NotifyOfPropertyChange(() => this.Photos);
            }
        }

        public WorkoutPhotoViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess)
            : base(navigationService, userService, localStorageAccess) {}
    }
}