﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkoutViewModel.cs" company="AZ" />
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using Microsoft.Phone.Shell;

namespace WorkoutDiary.WindowsPhoneApp.ViewModels.Workout
{
    #region

    using System;
    using System.Collections.ObjectModel;
    using System.Linq;

    using Caliburn.Micro;

    using LinqToTwitter;

    using Microsoft.Phone.Tasks;

    using Telerik.Windows.Controls;

    using WorkoutDiary.Core.Models;
    using WorkoutDiary.WindowsPhoneApp.Services;
    using WorkoutDiary.WindowsPhoneApp.ViewModels.Training;

    #endregion

    public class WorkoutViewModel : PivotViewModelBase
    {
        private readonly WorkoutDetailViewModel detailViewModel;

        private readonly WorkoutMeasurmentViewModel measurmentViewModel;

        private readonly WorkoutExerciseViewModel exerciseViewModel;
        
        private readonly IPhotoService photoService;

        private readonly WorkoutPhotoViewModel photoViewModel;

        private readonly IWorkoutService workoutService;

        private readonly IExerciseService exerciseService;

        private bool isCreate;

        private Workout model;

        public WorkoutViewModel(
            INavigationService navigationService,
            IUserService userService,
            ILocalStorageAccess localStorageAccess,
            WorkoutDetailViewModel workoutDetailViewModel,
            WorkoutPhotoViewModel workoutPhotoViewModel,
            WorkoutMeasurmentViewModel workoutMeasurmentViewModel,
            WorkoutExerciseViewModel workoutExerciseViewModel,
            IExerciseService exerciseService,
            IPhotoService photoService,
            IWorkoutService workoutService)
            : base(navigationService, userService, localStorageAccess)
        {
            this.detailViewModel = workoutDetailViewModel;
            this.photoViewModel = workoutPhotoViewModel;
            this.exerciseViewModel = workoutExerciseViewModel;
            this.measurmentViewModel = workoutMeasurmentViewModel;
            this.photoService = photoService;
            this.workoutService = workoutService;
            this.exerciseService = exerciseService;
        }

        public string ModelID
        {
            get
            {
                return "";
            }
            set
            {
                if (value != string.Empty)
                {
                    this.SetWorkout(value);
                }
                else
                {
                    var e = new Workout
                    {
                        Photos = new List<Photo>(),
                        Measurments = new List<Measurment>(),
                        Exercises = new List<WorkoutExercise>(),
                        DateTime = DateTime.Now
                    };
                    this.Model = e;
                }
            }
        }

        public async void SetWorkout(string id)
        {
            var m = await this.workoutService.GetWorkout(id);
            foreach (var part in m.Exercises)
            {
                part.Exercise = await this.exerciseService.Get(part.ExerciseID);
            }
            this.Model = m;
        }

        public Workout Model
        {
            get
            {
                return this.model;
            }
            set
            {
                this.model = value;
                this.detailViewModel.Model = (RestDay)this.model;
                this.photoViewModel.Photos = new List<Photo>(value.Photos);
                this.measurmentViewModel.Measurments = new List<Measurment>(value.Measurments);
                this.exerciseViewModel.Exercises = new List<WorkoutExercise>(value.Exercises);
                var dl = Enum.GetValues(typeof(RatingEnum));
                var dll = (from object v in dl select v.ToString()).ToList();
                this.detailViewModel.Ratings = new ObservableCollection<RatingElem>(dll.Select(x => new RatingElem() { Value = dll.IndexOf(x), Name = x }));
                this.detailViewModel.SelectedRating = this.detailViewModel.Ratings.Last(x => x.Value == value.Rating);
                NotifyOfPropertyChange(() => this.Model);
            }
        }

        public bool IsCreate
        {
            get
            {
                return this.isCreate;
            }
            set
            {
                this.isCreate = value;
                this.detailViewModel.IsCreate = value;
                NotifyOfPropertyChange(() => this.IsCreate);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            this.detailViewModel.DisplayName = "Details";
            this.photoViewModel.DisplayName = "Photos";
            this.measurmentViewModel.DisplayName = "Measurments";
            this.exerciseViewModel.DisplayName = "Exercises";
            this.Items.Add(this.detailViewModel);
            this.Items.Add(this.photoViewModel);
            this.Items.Add(this.measurmentViewModel);
            this.Items.Add(this.exerciseViewModel);

            this.ActivateItem(this.detailViewModel);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            if (PhoneApplicationService.Current.State.ContainsKey("workout"))
            {
                this.Model = (Workout)PhoneApplicationService.Current.State["workout"];
                this.IsCreate = true;
                PhoneApplicationService.Current.State.Remove("workout");
            }
        }

        public void AddPhoto()
        {
            this.model.Photos = this.photoViewModel.Photos.ToList();
            this.model.Measurments = this.measurmentViewModel.Measurments.ToList();
            this.model.Exercises = this.exerciseViewModel.Exercises.ToList();
            this.model.UserID = this.LocalStorageAccess.GetInfoValue(Services.UserService.USER_ID).ToString();
            this.model.Rating = this.detailViewModel.SelectedRating.Value;
            PhoneApplicationService.Current.State["workout"] = this.model;
            var cameraCaptureTask = new CameraCaptureTask();
            cameraCaptureTask.Completed += cameraCaptureTask_Completed;
            cameraCaptureTask.Show();
        }

        public async void AddMeasurment()
        {
            var unitInput = await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide measurment unit");
            var valueInput = await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide measurment value");
            var m = new Measurment()
            {
                Value = Convert.ToDouble(valueInput.Text),
                Unit = (MeasurmentUnit)Enum.Parse(typeof(MeasurmentUnit), unitInput.Text)
            };
            var t = new List<Measurment>();
            foreach (var ph in this.measurmentViewModel.Measurments)
            {
                t.Add(ph);
            }
            t.Add(m);

            this.measurmentViewModel.Measurments = t;
        }

        public void AddExercise()
        {
            this.model.Photos = this.photoViewModel.Photos.ToList();
            this.model.Measurments = this.measurmentViewModel.Measurments.ToList();
            this.model.Exercises = this.exerciseViewModel.Exercises.ToList();
            this.model.UserID = this.LocalStorageAccess.GetInfoValue(Services.UserService.USER_ID).ToString();
            this.model.Rating = this.detailViewModel.SelectedRating.Value;
            PhoneApplicationService.Current.State["workout"] = this.model;
            this.NavigationService.UriFor<WorkoutNewExerciseViewModel>().Navigate();
        }

        public void Save()
        {
            this.model.Photos = this.photoViewModel.Photos.ToList();
            this.model.Measurments = this.measurmentViewModel.Measurments.ToList();
            this.model.Exercises = this.exerciseViewModel.Exercises.ToList();
            this.model.UserID = this.LocalStorageAccess.GetInfoValue(Services.UserService.USER_ID).ToString();
            this.model.Rating = this.detailViewModel.SelectedRating.Value;
            this.workoutService.Insert(this.model);
            this.NavigationService.GoBack();
        }

        private async void cameraCaptureTask_Completed(object sender, PhotoResult e)
        {
            var titleInput =
                await RadInputPrompt.ShowAsync("Additional info", MessageBoxButtons.OK, "Please provide photo title");
            var photo = await this.photoService.Insert(e.ChosenPhoto);
            photo.Title = titleInput.Text;
            var t = new List<Photo>();
            foreach (var ph in this.photoViewModel.Photos)
            {
                t.Add(ph);
            }
            t.Add(photo);

            this.photoViewModel.Photos = t;
        }
    }
}